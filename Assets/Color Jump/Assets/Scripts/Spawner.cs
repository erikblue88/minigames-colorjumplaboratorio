﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject obstacle;
    public float increaseSpeedBy, maxObstacleSpeed;

    [HideInInspector]
    public int spawnedObstacles = 0;

    private float speed;

	void Start () {
        Spawn();        //First spawn
        speed = increaseSpeedBy;
    }

    public void Spawn()
    {
        GameObject obst = Instantiate(obstacle, transform.position, Quaternion.identity);       //Spawns a random obstacle to the spawner's position with the same rotation

        obst.transform.Rotate(Vector3.up, 180f);

        for (int i = 0; i < obst.transform.childCount; i++)
        {
            if (obst.transform.GetChild(i).CompareTag("Obstacle"))
            {
                if (spawnedObstacles % 2 == 0)
                {
                    obst.transform.GetChild(i).GetComponent<Obstacle>().speed *= -1f;     //Reverse direction
                    obst.transform.GetChild(i).GetComponent<Obstacle>().speed += speed;     //Obstacles move right
                }
                else
                {
                    obst.transform.GetChild(i).GetComponent<Obstacle>().speed -= speed;     //Obstacles move left
                }
            }
        }

        if (maxObstacleSpeed > speed)
            speed += increaseSpeedBy;       //Increases speed

        spawnedObstacles++;     //Counts the spawn rounds
    }
}
