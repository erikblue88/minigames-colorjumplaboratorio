﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPos : MonoBehaviour
{
    public GameObject obstacle;
    public Transform startPos;

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Obstacle"))       //If collides with an obstacle
        {
            other.transform.position = startPos.position;       //Obstacle's position is set to the startPosition
            FindObjectOfType<ColorManager>().SetRandomColor(other.GetComponent<Renderer>());        //Selects a random color for the obstacle
        }
    }
}
