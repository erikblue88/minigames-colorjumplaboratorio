﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleHolder : MonoBehaviour
{
    public float speed;
    public int spaceBetweenObstacles = 4;

    private Vector3 nextPos;

    [HideInInspector]
    public bool canMove = false;

    void Update()
    {
        if (canMove)
            transform.position = Vector3.MoveTowards(transform.position, nextPos, speed * Time.deltaTime);      //Makes the obstacles move towards the player
    }

    public void StartMoving()
    {
        canMove = true;
        nextPos = transform.position;
        nextPos.z -= spaceBetweenObstacles;
    }

    public void StopChildrenMovement()
    {
        for (int i = 0; i < transform.childCount-4; i++)
            transform.GetChild(i).GetComponent<Rigidbody>().Sleep();        //Makes the children stop
    }
}
