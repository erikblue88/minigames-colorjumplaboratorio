﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject tokenParticle, obstacleParticle;

    private Animation playerAnim;
    private Renderer playerRend, particleRend;
    private bool canJump = true;

    void Start()
    {
        //Initializations
        playerAnim = GetComponent<Animation>();
        playerRend = GetComponent<Renderer>();
        particleRend = GetComponent<ParticleSystemRenderer>();

        FindObjectOfType<ColorManager>().SetRandomColor(playerRend);        //Selects a random color for the player
        FindObjectOfType<ColorManager>().ChangeColor(particleRend, playerRend);     //Changes the particle's color
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && canJump && !FindObjectOfType<GameManager>().gameIsOver && FindObjectOfType<GameManager>().gameHasStarted && !FindObjectOfType<GameManager>().preguntaActiva)        //On click (when game is started and not over yet)
        {
            canJump = false;

            foreach (GameObject obj in GameObject.FindGameObjectsWithTag("ObstacleHolder"))
                obj.GetComponent<ObstacleHolder>().StartMoving();       //Starts the obstacles

            playerAnim.Play("JumpAnim");        //Plays animation
            FindObjectOfType<Spawner>().Spawn();        //Spawns new obstacles
        }
    }

    public void OnTriggerEnter (Collider other)
    {
        if (!FindObjectOfType<GameManager>().gameIsOver && FindObjectOfType<GameManager>().gameHasStarted)      //If the game started and is not over yet
        {
            if (other.CompareTag("Obstacle"))       //If player collides with an obstacle
            {
                canJump = true;

                other.GetComponentInParent<ObstacleHolder>().StopChildrenMovement();        //Stops obstacles

                if (FindObjectOfType<ColorManager>().CompareColor(playerRend, other.GetComponent<Renderer>()))      //If they have the same color
                {
                    FindObjectOfType<ScoreManager>().IncrementScore();      //Increments score

                    GameObject obstPar = Instantiate(obstacleParticle, other.transform.position, Quaternion.identity);      //Spawns obstacleParticle to the collision's position
                    FindObjectOfType<ColorManager>().ChangeColor(obstPar.GetComponent<ParticleSystemRenderer>(), playerRend);       //Changes obstacleParticle's color to identical color of the players
                    Destroy(obstPar, 3.2f);     //Destroys obstacleParticle after x seconds

                    FindObjectOfType<ColorManager>().SetRandomColor(playerRend);        //Selects a random color for the player
                    FindObjectOfType<ColorManager>().ChangeColor(particleRend, playerRend);     //Changes the particles color
                }
                else         //If they have different color
                {
                    FindObjectOfType<GameManager>().EndPanelActivation();       //Activates endPanel
                }
            }
            if (other.CompareTag("Fall"))       //If player collides with 'Fall'
            {
                GetComponent<Animation>().Play("FallAnim");     //Plays animation
                FindObjectOfType<GameManager>().Invoke("EndPanelActivation", 0.7f);     //Activates endPanel after x seconds
            }
            if (other.CompareTag("Token"))      //If player collides with a token
            {
                FindObjectOfType<ScoreManager>().IncrementToken();      //Increments token
                Destroy(other.gameObject);      //Destroys token
                Destroy(Instantiate(tokenParticle, transform.position, Quaternion.identity), 1.2f);     //Instantiates tokenParticle and destroys it after x seconds
            }

        }
    }
}
