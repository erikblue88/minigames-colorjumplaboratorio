﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimation : MonoBehaviour
{
    private Animation anim;

    void Start()
    {
        anim = GetComponent<Animation>();       //Initializes Animation

        switch (Random.Range(0, 2))
        {
            case 0:
                Invoke("Anim1", Random.Range(10f, 20f));        //Plays anim1
                break;
            case 1:
                Invoke("Anim2", Random.Range(10f, 20f));        //Plays anim2
                break;
        } 
    }

    public void Anim1()
    {
        anim.Play("CamToPos1");
        switch (Random.Range(0, 2))
        {
            case 0:
                Invoke("Anim1", Random.Range(20f, 30f));
                break;
            case 1:
                Invoke("Anim2", Random.Range(20f, 30f));
                break;
        }
    }

    public void Anim2()
    {
        anim.Play("CamToPos2");
        switch (Random.Range(0, 2))
        {
            case 0:
                Invoke("Anim1", Random.Range(20f, 30f));
                break;
            case 1:
                Invoke("Anim2", Random.Range(20f, 30f));
                break;
        }
    }
}
