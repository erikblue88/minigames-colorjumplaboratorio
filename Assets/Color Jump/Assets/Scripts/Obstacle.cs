﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public float speed;
    public int spaceBetweenObstacles = 4, tokenSpawnFrequency = 4;
    public GameObject token;

    private Rigidbody rb;
    private Vector3 nextPos;

    [HideInInspector]
    public bool canMove = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();     //Initializes rigidbody
        rb.AddForce(Vector3.right * speed);     //Adds a force to the obstacle to move right or left
        FindObjectOfType<ColorManager>().SetRandomColor(GetComponent<Renderer>());      //Selects a random color for obstacle

        if (Random.Range(0, tokenSpawnFrequency) == 0)      //If it is time to spawn a token
            Instantiate(token, transform);      //Spawns token
    }
}
