﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Cloud : Singleton<Cloud>
{
    [HideInInspector] public DatosUser user;

    private int idJuego = 1;
    private string cantidad = "10";
    private string _emailTemp;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void DownloadLeaderboard()
    {
        StartCoroutine(downloadLeaderBoard(URLs.leaderboard + "/1/" + cantidad));
    }

    public void DownloadPreguntas()
    {
        StartCoroutine(downloadPreguntas(URLs.preguntas + "/" + 11));
    }

    public void Login(string email)
    {
        WWWForm form = new WWWForm();
        form.AddField("email", email);
        form.AddField("id_juego", idJuego);
        _emailTemp = email;
        //string productList = JsonHelper.ToJson(UserData_DLFile.instance.product_List.ToArray());
        //char[] MyChar = { '{', '"', 'I', 't', 'e', 'm', 's', ':' };
        //string NewString = productList.TrimStart(MyChar);
        //char[] MyCharEnd = { '}' };
        //string finalData = NewString.TrimEnd(MyCharEnd);
        //form.AddField("product_list", finalData);

        StartCoroutine(login(form, URLs.login));
    }

    public void Registro(string nombre, string apellido, string email)
    {
        WWWForm form = new WWWForm();
        form.AddField("nombre", nombre);
        form.AddField("apellidos", apellido);
        form.AddField("email", email);
        StartCoroutine(registro(form, URLs.registro));
    }

    public void Respuesta(string idPregunta, string correcta)
    {
        WWWForm form = new WWWForm();
        form.AddField("id_juego", idJuego);
        form.AddField("id_pregunta", idPregunta);
        form.AddField("id_usuario", user.id_usuario);
        form.AddField("correcta", correcta);
        StartCoroutine(respuesta(form, URLs.respuesta));
    }

    public void ReportScore(int score)
    {
        WWWForm form = new WWWForm();
        form.AddField("id_juego", idJuego);
        form.AddField("id_usuario", user.id_usuario);
        form.AddField("score", score);
        StartCoroutine(reportScore(form, URLs.score));
    }

    #region POST
    IEnumerator login(WWWForm form, string url)
    {
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            www.SetRequestHeader("Accept", "application/json");
            //www.SetRequestHeader("Authorization", "Bearer " + token);
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                //ViewMessage message = JsonUtility.FromJson<ViewMessage>(www.error);
                //LoginControl.instance.SetMessage(message.mensaje, true);
            }
            else
            {
                //TODO Lo que se tenga que hacer si es exitoso
                print(www.downloadHandler.text);
                string mystring = www.downloadHandler.text.Substring(1, www.downloadHandler.text.Length - 1);
                ViewUser message = JsonUtility.FromJson<ViewUser>(mystring);
                if(string.IsNullOrEmpty(user.token))
                {
                    LoginControl.instance.SetMessage(message.mensaje, !message.exito);
                }
                if(message.exito)
                {
                    user = message.datos;
                    user.email = _emailTemp;
                    PlayerPrefs.SetString("ID_USER", user.id_usuario);
                    PlayerPrefs.SetString("TOKEN", user.token);
                    PlayerPrefs.SetString("EMAIL", user.email);
                    OpenGame();
                }
                //ViewTemplateResponse vtr = JsonUtility.FromJson<ViewTemplateResponse>(www.downloadHandler.text);
            }
        }
    }

    IEnumerator registro(WWWForm form, string url)
    {
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            www.SetRequestHeader("Accept", "application/json");
            //www.SetRequestHeader("Authorization", "Bearer " + token);
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                //ViewMessage message = JsonUtility.FromJson<ViewMessage>(www.error);
                RegistroControl.instance.SetMessage(www.error, true);
            }
            else
            {
                //TODO Lo que se tenga que hacer si es exitoso
                print(www.downloadHandler.text);
                string mystring = www.downloadHandler.text.Substring(1, www.downloadHandler.text.Length - 1);
                ViewMessage message = JsonUtility.FromJson<ViewMessage>(mystring);
                RegistroControl.instance.SetMessage(message.mensaje, !message.exito);
            }
        }
    }

    IEnumerator respuesta(WWWForm form, string url)
    {
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            print(url);
            www.SetRequestHeader("Accept", "application/json");
            //www.SetRequestHeader("Authorization", "Bearer " + token);
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                //ViewMessage message = JsonUtility.FromJson<ViewMessage>(www.error);
                //LoginControl.instance.SetMessage(message.mensaje, true);
            }
            else
            {
                //TODO Lo que se tenga que hacer si es exitoso
                print(www.downloadHandler.text);
                string mystring = www.downloadHandler.text.Substring(1, www.downloadHandler.text.Length - 1);
                //ViewMessage message = JsonUtility.FromJson<ViewMessage>(mystring);
            }
        }
    }

    IEnumerator reportScore(WWWForm form, string url)
    {
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            print(url);
            www.SetRequestHeader("Accept", "application/json");
            //www.SetRequestHeader("Authorization", "Bearer " + token);
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                //ViewMessage message = JsonUtility.FromJson<ViewMessage>(www.error);
                //LoginControl.instance.SetMessage(message.mensaje, true);
            }
            else
            {
                //TODO Lo que se tenga que hacer si es exitoso
                print(www.downloadHandler.text);
                string mystring = www.downloadHandler.text.Substring(1, www.downloadHandler.text.Length - 1);
                //ViewMessage message = JsonUtility.FromJson<ViewMessage>(mystring);
            }
        }
    }

    #endregion //POST

    #region GET

    IEnumerator downloadLeaderBoard(string url)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            print(url);
            webRequest.SetRequestHeader("Accept", "application/json");
            //webRequest.SetRequestHeader("Authorization", "Bearer " + token);
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            if (!string.IsNullOrEmpty(webRequest.error))
            {
                Debug.Log("Error: " + webRequest.error);
            }
            else
            {
                print(webRequest.downloadHandler.text);
                string mystring = webRequest.downloadHandler.text.Substring(1, webRequest.downloadHandler.text.Length - 1);
                ViewLeaderBoard vl = JsonUtility.FromJson<ViewLeaderBoard>(mystring);
                LeaderboardControl.instance.ArmaLeaderboard(vl);
            }
        }
    }

    IEnumerator downloadPreguntas(string url)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            print(url);
            webRequest.SetRequestHeader("Accept", "application/json");
            //webRequest.SetRequestHeader("Authorization", "Bearer " + token);
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            if (!string.IsNullOrEmpty(webRequest.error))
            {
                Debug.Log("Error: " + webRequest.error);
                //TempString 
                //string mystring;
                //mystring = "{\"exito\": true,\"mensaje\": \"Preguntas cargadas con exito\",\"datos\": {\"tickets\": [{\"id\": 1,\"pregunta\": \"¿AAAAAAAAA?\",\"opciones\": [{\"id\": 1,\"opcion\": \"Si\",\"valor\": 10},{\"id\": 2,\"opcion\": \"No\",\"valor\": 5}, {\"id\": 3,\"opcion\": \"Tal vez\",\"valor\": 11}],\"correcta\": 1},{\"id\": 2,\"pregunta\": \"¿BBBBBBBBBBB?\",\"opciones\": [{\"id\": 10,\"opcion\": \"No\",\"valor\": 5},{\"id\": 5,\"opcion\": \"Si\",\"valor\": 0}, {\"id\": 11,\"opcion\": \"No se\",\"valor\": 6}],\"correcta\" : 10},{\"id\": 3,\"pregunta\": \"¿CCCCCCCCCCCCC?\",\"opciones\": [{\"id\": 6,\"opcion\": \"Si\",\"valor\": 50},{\"id\": 7,\"opcion\": \"No\",\"valor\": 10},{\"id\": 8,\"opcion\": \"creo que Si\",\"valor\": 0}],\"correcta\" : 6}]}}";
                //ViewPreguntas vp = JsonUtility.FromJson<ViewPreguntas>(mystring);
                //PreguntasControl.instance.tickets = vp.datos.tickets;
                ////// Borrar todo esto, va solo cuando es exitoso
            }
            else
            {
                print(webRequest.downloadHandler.text);
                string mystring = webRequest.downloadHandler.text.Substring(1, webRequest.downloadHandler.text.Length - 1);
                //TempString
                //mystring = "{\"exito\": true,\"mensaje\": \"Preguntas cargadas con exito\",\"datos\": {\"tickets\": [{\"id\": 1,\"pregunta\": \"¿AAAAAAAAA?\",\"opciones\": [{\"id\": 1,\"opcion\": \"Si\",\"valor\": 10},{\"id\": 2,\"opcion\": \"No\",\"valor\": 5}],\"correcta\": 1},{\"id\": 2,\"pregunta\": \"¿BBBBBBBBBBB?\",\"opciones\": [{\"id\": 10,\"opcion\": \"No\",\"valor\": 5},{\"id\": 5,\"opcion\": \"Si\",\"valor\": 0}],\"correcta\" : 10},{\"id\": 3,\"pregunta\": \"¿CCCCCCCCCCCCC?\",\"opciones\": [{\"id\": 6,\"opcion\": \"Si\",\"valor\": 50},{\"id\": 7,\"opcion\": \"No\",\"valor\": 10},{\"id\": 8,\"opcion\": \"Si\",\"valor\": 0}],\"correcta\" : 6}]}}";
                ViewPreguntas vp = JsonUtility.FromJson<ViewPreguntas>(mystring);
                PreguntasControl.instance.tickets = vp.datos;
            }
        }
    }

    //public float timer;
    //public bool runTimer;

    //public void Update()
    //{
    //    if (runTimer)
    //    {
    //        timer += Time.deltaTime;
    //    }
    //}

    #endregion //GET

    public void OpenGame()
    {
        SceneManager.LoadScene(2);
    }
}
