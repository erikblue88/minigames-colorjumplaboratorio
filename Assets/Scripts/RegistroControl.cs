﻿using System.Text.RegularExpressions;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class RegistroControl : Singleton<RegistroControl>
{
    [SerializeField] private TMP_InputField _nombreField;
    [SerializeField] private TMP_InputField _apellidoField;
    [SerializeField] private TMP_InputField _emailField;
    [SerializeField] private TextMeshProUGUI _statusText;
    [SerializeField] private GameObject _succesImage;

    public const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    public void Registro()
    {
        bool emailCorrecto, nombreCorrecto, apellidoCorrecto;
        if (validateName(_nombreField.text))
        {
            nombreCorrecto = true;
        }
        else
        {
            nombreCorrecto = false;
            SetMessage("Ingresa un nombre", true);
            return;
        }
        if (validateName(_apellidoField.text))
        {
            apellidoCorrecto = true;
        }
        else
        {
            apellidoCorrecto = false;
            SetMessage("Ingresa un apellido", true);
            return;
        }
        if (validateEmail(_emailField.text))
        {
            emailCorrecto = true;
        }
        else
        {
            emailCorrecto = false;
            SetMessage("Ingresa un correo válido", true);
            return;
        }
        if(nombreCorrecto && apellidoCorrecto && emailCorrecto)
        {
            Cloud.instance.Registro(_nombreField.text, _apellidoField.text, _emailField.text);
        }
    }

    public void SetMessage(string message, bool isError)
    {
        _statusText.color = isError ? Color.red : Color.green;
        _statusText.text = message;
        if(!isError)
        {
            _statusText.gameObject.SetActive(false);
            _succesImage.SetActive(true);
        }
        else
        {
            _statusText.gameObject.SetActive(true);
            _succesImage.SetActive(false);
        }
    }

    public static bool validateEmail(string email)
    {
        if (email != null)
            return Regex.IsMatch(email, MatchEmailPattern);
        else
            return false;
    }

    public static bool validateName(string email)
    {
        if (string.IsNullOrEmpty(email))
            return false;
        else
            return true;
    }
}
