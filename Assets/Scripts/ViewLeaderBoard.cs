﻿// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
using System.Collections.Generic;

[System.Serializable]
public class TablaPosicion
{
    public string id;
    public string id_usuario;
    public string id_juego;
    public string score;
    public string fecha_creacion;
    public string email;
    public string nombre;
}

[System.Serializable]
public class Datos
{
    public List<TablaPosicion> tabla_posicion;
}

[System.Serializable]
public class ViewLeaderBoard
{
    public bool exito;
    public string mensaje;
    public Datos datos;
}