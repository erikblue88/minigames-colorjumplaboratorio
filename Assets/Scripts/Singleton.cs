﻿// ************************************************************************ 
// File Name:   Singleton.cs 
// Purpose:    	Singleton pattern
// Project:		MiniGamesLaboratorio
// Author:      Erik Marmolejo Ensastegui
// Copyright: 	2021 Ensastegeek
// ************************************************************************ 

// ************************************************************************ 
// Imports 
// ************************************************************************ 
using UnityEngine;

// ************************************************************************ 
// Class: Singleton
// ************************************************************************ 
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    // ********************************************************************
    // Static Data Members 
    // ********************************************************************
    protected static T s_instance;

    // ********************************************************************
    // Properties 
    // ********************************************************************
    public static T instance
    {
        get
        {
            // Check if the instance already exists in the scene
            if (s_instance == null)
                s_instance = (T)FindObjectOfType(typeof(T));
            return s_instance;
        }
    }
}

