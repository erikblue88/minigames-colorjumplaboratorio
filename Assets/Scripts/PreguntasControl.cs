﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PreguntasControl : Singleton<PreguntasControl>
{
    public List<Ticket> tickets;
    public GameObject panelCorrecto;
    public GameObject panelIncorrecto;

    public Animator animOp1;
    public Animator animOp2;
    public Animator animOp3;

    [HideInInspector] public int score;

    [SerializeField] private TextMeshProUGUI pregunta;
    [SerializeField] private TextMeshProUGUI opcion1;
    [SerializeField] private TextMeshProUGUI opcion2;
    [SerializeField] private TextMeshProUGUI opcion3;

    // Start is called before the first frame update
    public void Start()
    {
        Cloud.instance.DownloadPreguntas();
        score = 0;
    }

    public void ArmaPregunta()
    {
        int nPregunta = Random.RandomRange(0, tickets.Count);
        pregunta.text = tickets[nPregunta].pregunta;
        opcion1.text = tickets[nPregunta].opciones[0].texto;
        opcion1.transform.parent.GetComponent<DataRespueta>().idPregunta = tickets[nPregunta].id;
        opcion1.transform.parent.GetComponent<DataRespueta>().idRespuesta = tickets[nPregunta].opciones[0].id;
        opcion1.transform.parent.GetComponent<DataRespueta>().idCorrecta = tickets[nPregunta].opciones[0].correcta;
        opcion2.text = tickets[nPregunta].opciones[1].texto;
        opcion2.transform.parent.GetComponent<DataRespueta>().idPregunta = tickets[nPregunta].id;
        opcion2.transform.parent.GetComponent<DataRespueta>().idRespuesta = tickets[nPregunta].opciones[1].id;
        opcion2.transform.parent.GetComponent<DataRespueta>().idCorrecta = tickets[nPregunta].opciones[1].correcta;
        opcion3.text = tickets[nPregunta].opciones[2].texto;
        opcion3.transform.parent.GetComponent<DataRespueta>().idPregunta = tickets[nPregunta].id;
        opcion3.transform.parent.GetComponent<DataRespueta>().idRespuesta = tickets[nPregunta].opciones[2].id;
        opcion3.transform.parent.GetComponent<DataRespueta>().idCorrecta = tickets[nPregunta].opciones[2].correcta;
        tickets.RemoveAt(nPregunta);
    }

    public void CompruebaRespuesta(DataRespueta respuesta)
    {
        if (respuesta.idCorrecta == "1")
        {
            score++;
            FindObjectOfType<ScoreManager>().IncrementScore(10);
            Cloud.instance.Respuesta(respuesta.idPregunta, "1");
            panelCorrecto.SetActive(true);
            FindObjectOfType<AudioManager>().RespuestaCorrecta();
        }
        else
        {
            Cloud.instance.Respuesta(respuesta.idPregunta, "0");
            panelIncorrecto.SetActive(true);
            FindObjectOfType<AudioManager>().RespuestaIncorrecta();
            if (opcion1.transform.parent.GetComponent<DataRespueta>().idCorrecta == "1")
                animOp1.SetTrigger("correcta");
            else if (opcion2.transform.parent.GetComponent<DataRespueta>().idCorrecta == "1")
                animOp2.SetTrigger("correcta");
            else if (opcion3.transform.parent.GetComponent<DataRespueta>().idCorrecta == "1")
                animOp3.SetTrigger("correcta");
        }
        //if (tickets.Count == 0)
        //{
        //    Cloud.instance.ReportScore(score);
        //    SceneManager.LoadScene(3);
        //}
    }
}
