﻿// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
using System.Collections.Generic;

// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
[System.Serializable]
public class Opcione
{
    public string id;
    public string id_pregunta;
    public string texto;
    public string correcta;
    public string estatus;
    public string fecha_creacion;
}

[System.Serializable]
public class Ticket
{
    public string id;
    public string pregunta;
    public string info;
    public string estatus;
    public string valor;
    public string tipo;
    public string fecha_creacion;
    public List<Opcione> opciones;
}

[System.Serializable]
public class ViewPreguntas
{
    public bool exito;
    public string mensaje;
    public List<Ticket> datos;
}