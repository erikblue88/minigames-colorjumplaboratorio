﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PasaEscena : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //PlayerPrefs.SetString("ID_USER", "");
        //PlayerPrefs.SetString("TOKEN", "");
        //PlayerPrefs.SetString("EMAIL", "");
        Cloud.instance.user.id_usuario = PlayerPrefs.GetString("ID_USER", "");
        Cloud.instance.user.token = PlayerPrefs.GetString("TOKEN", "");
        Cloud.instance.user.email = PlayerPrefs.GetString("EMAIL", "");
        if (string.IsNullOrEmpty(Cloud.instance.user.id_usuario))
            SceneManager.LoadScene(1);
        else
        {
            Cloud.instance.Login(Cloud.instance.user.email);
        }
    }
}
